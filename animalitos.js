export const animalitos = {
  '0': {
    name: 'Delfin',
    image: 'assets/Animales/37.png',
  },
  '10': {
    name: 'Tigre',
    image: 'assets/Animales/10.png',
  },
  '11': {
    name: 'Gato',
    image: 'assets/Animales/11.png',
  },
  '12': {
    name: 'Caballo',
    image: 'assets/Animales/12.png',
  },
  '13': {
    name: 'Mono',
    image: 'assets/Animales/13.png',
  },
  '14': {
    name: 'Paloma',
    image: 'assets/Animales/14.png',
  },
  '15': {
    name: 'Zorro',
    image: 'assets/Animales/15.png',
  },
  '16': {
    name: 'Oso',
    image: 'assets/Animales/16.png',
  },
  '17': {
    name: 'Pavo',
    image: 'assets/Animales/17.png',
  },
  '18': {
    name: 'Burro',
    image: 'assets/Animales/18.png',
  },
  '19': {
    name: 'Cabra',
    image: 'assets/Animales/19.png',
  },
  '20': {
    name: 'Cochino',
    image: 'assets/Animales/20.png',
  },
  '21': {
    name: 'Gallo',
    image: 'assets/Animales/21.png',
  },
  '22': {
    name: 'Camello',
    image: 'assets/Animales/22.png',
  },
  '23': {
    name: 'Cebra',
    image: 'assets/Animales/23.png',
  },
  '24': {
    name: 'Iguana',
    image: 'assets/Animales/24.png',
  },
  '25': {
    name: 'Gallina',
    image: 'assets/Animales/25.png',
  },
  '26': {
    name: 'Vaca',
    image: 'assets/Animales/26.png',
  },
  '27': {
    name: 'Perro',
    image: 'assets/Animales/27.png',
  },
  '28': {
    name: 'Zamuro',
    image: 'assets/Animales/28.png',
  },
  '29': {
    name: 'Elefante',
    image: 'assets/Animales/29.png',
  },
  '30': {
    name: 'Caimán',
    image: 'assets/Animales/30.png',
  },
  '31': {
    name: 'Lapa',
    image: 'assets/Animales/31.png',
  },
  '32': {
    name: 'Ardilla',
    image: 'assets/Animales/32.png',
  },
  '33': {
    name: 'Pescado',
    image: 'assets/Animales/33.png',
  },
  '34': {
    name: 'Venado',
    image: 'assets/Animales/34.png',
  },
  '35': {
    name: 'Jirafa',
    image: 'assets/Animales/35.png',
  },
  '36': {
    name: 'Culebra',
    image: 'assets/Animales/36.png',
  },
  '00': {
    name: 'Ballena',
    image: 'assets/Animales/38.png',
  },
  '01': {
    name: 'Carnero',
    image: 'assets/Animales/01.png',
  },
  '02': {
    name: 'Toro',
    image: 'assets/Animales/02.png',
  },
  '03': {
    name: 'Ciempiés',
    image: 'assets/Animales/03.png',
  },
  '04': {
    name: 'Alacrán',
    image: 'assets/Animales/04.png',
  },
  '05': {
    name: 'León',
    image: 'assets/Animales/05.png',
  },
  '06': {
    name: 'Sapo',
    image: 'assets/Animales/06.png',
  },
  '07': {
    name: 'Loro',
    image: 'assets/Animales/07.png',
  },
  '08': {
    name: 'Ratón',
    image: 'assets/Animales/08.png',
  },
  '09': {
    name: 'Aguila',
    image: 'assets/Animales/09.png',
  },
};
