import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router';
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/css/index.css';
import "bootstrap/dist/css/bootstrap.css";

createApp(App)
    .use(router)
    .use(VueLoading)
    .mount('#app')

import "bootstrap/dist/js/bootstrap.js";

