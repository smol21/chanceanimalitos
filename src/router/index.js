import { createRouter, createWebHistory } from 'vue-router'
import Home from "../views/Home/Home.vue";
import  ResultadosChance from '../views/ResultadosChance.vue'
import ResultadosAnimalitos from '../views/ResultadosAnimalitos.vue'
import Contacto from "../views/Contacto/contacto.vue";
const routes = [ 
    {
        path: "/",
        name: "home",
        component: Home,
      },
    {
        path: '/resultadosChance',
        name: 'Resultados Chance',
        component: ResultadosChance
    },
    {
        path: '/resultadosAnimalitos',
        name: 'Resultados Animalitos',
        component: ResultadosAnimalitos
    },
    {
        path: '/contacto',
        name: 'Contacto',
        component: Contacto
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;

