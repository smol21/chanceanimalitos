import { helpers,required, email, maxLength, minLength } from "@vuelidate/validators";
import { SoloLetras, SoloNumeros } from '../ValidacionEspeciales';
export const validarContacto  = {
    nombre: { 
        required: helpers.withMessage('Campo requerido.', required),
        SoloLetras: helpers.withMessage('Solo puede introducir letras.', SoloLetras) },
    email: { 
        required: helpers.withMessage('Campo requerido.', required),
        email: helpers.withMessage('Formato de email inválido.',email),
        maxLength: helpers.withMessage('La longitud máxima permitida es de 150 caracteres.', maxLength(150))  },
    telefono: { 
        required: helpers.withMessage('Campo requerido.', required),
        SoloNumeros: helpers.withMessage('Solo puede introducir números.', SoloNumeros),
        maxLength: helpers.withMessage('La longitud máxima permitida es de 15 caracteres.', maxLength(15)) },  
    mensaje: { 
        required: helpers.withMessage('Campo requerido.', required),
    }
}
